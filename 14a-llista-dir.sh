#! /bin/bash
# @edt ASIX-M01
# Febrer 2022
# 
# validar existeix un arg
# dir es un directori 
# fer un ls del directori

ERR_NARGS=1
ERR_NODIR=2 

# validar num args 
if [ $# -ne 1 ]; then
  echo "Error: Num args incorrecte"
  echo "Usage: $0 llista dir" 
  exit $ERR_NARGS
fi

# validar si es un dir 

if [ ! -d $1  ]; then
  echo "ERROR: $1 no és un directori"
  echo "usage: $0 llista dir"
  exit $ERR_NODIR
fi 

# xixa 

dir=$1 
ls $dir

exit 0 
