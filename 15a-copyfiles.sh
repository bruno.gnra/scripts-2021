#! /bin/bash
# @edt ASIX-M01 CURS 2021-2022
# Febrero 14
# Descripción: file origen dirdesti 

# validar 2 args
# primer arg es un file 
# segon es un dir existen 
#-------------------------------

ERR_NARGS=1
ERR_NFILE=2
ERR_NDIR=3
OK=0 

# validem args 

if [ $# -ne 2 ] 
then 
  echo "Error: Numero de args incorrecto" 
  echo "Usage: $0 file dir-desti" 
  exit $ERR_NARGS
fi 

# validar si es un file
file=$1 

if [ ! -f $file ]
then 
  echo "ERROR: $file no és un file" 2>>/dev/null
  echo "Usage: $0 file dir-desti"
  exit $ERR_NFILE
fi 

# validar si es un dir 
dir=$2

if [ ! -d $2 ] 
then 
  echo "ERROR: $dir no és un directori" 2>>/dev/null
  echo "Usage: $0 file dir-desti" 
  exit $ERR_NDIR
fi 

cp $file $dir

exit $OK 



