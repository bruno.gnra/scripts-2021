#! /bin/bash
# @edt ASIX-M01 CURS 2021-2022
# Febrero 14
# Descripción: exemples bucle for 


#listar todos los login numerados 

lista_logins=$(cut -d: -f1 /etc/passwd | sort )
num=1

for login in $lista_logins
do 
  echo "$num: $login"
  ((num++))
done 
exit 0 


# llistar numerat els noms dels files del dir actiu 

lista=$(ls)
num=1

for elem in $lista
do 
  echo "$num: $elem"
  ((num++))
done 
exit 0 

#-------------------------------
num=1

for arg in $*
do 
  echo "$num:$arg"
  ((num++))
  #num=$((num+1))
done 
exit 0 

#-------------------------------
# iterar per una llista de args

for arg in "$@"
do 
  echo $arg
done
exit 0 

#----------------------------------
# iterar per una llista de args

for arg in $* 
do 
  echo $arg
done
exit 0 

#----------------------------------
# iterar pel valor d'una variable 

lista=$(ls)

for nom in $lista
do 
  echo $nom
done 
exit 0 

#----------------------------------
# iterar pel valor d'una variable 

for nom in "pere" "pau" "marta" "anna"
do
  echo "$nom"
done 
exit 0 

#----------------------------------
# iterar per un conjunt d'elements 
for nom in "pere pau marta anna"
do
  echo "$nom"
done 
exit 0 


