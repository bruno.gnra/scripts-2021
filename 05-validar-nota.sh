#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022

# Validar nota: Suspès, aprovat
#--------------------------------

ERR_NARGS=1 
ERR_NOTA=2

# 1) V+alidem si existe un arg 

if [ $# -ne 1 ] 
then
  echo "Error: numero de args incorrecto" 
  echo "Usage: $0 nota" 
  exit $ERR_NARGS
fi 

# 2) Validar rang nota 

nota=$1 

if ! [ $nota -ge 0 -a $nota -le 10 ] 
then
  echo "Error: $nota nota no valid" 
  echo "nota pren valors de 0 a 10" 
  echo "Usage: $0 nota" 
  exit $ERR_NOTA
fi 

# xixa 

if [ $nota -lt 5 ] 
then 
  echo " La nota es: $nota, estas suspès." 
else
  echo " La nota es: $nota, estas aprobado." 
fi 

exit 0 
