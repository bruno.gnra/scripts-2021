#! /bin/bash
# @edt ASIX-M01
# Febrer 2022

# validar existeix un arg
# dir es un directori 
# fer un ls del directori
# numerar un a un els noms de fixers 

ERR_NARGS=1
ERR_NODIR=2 

# validar num args 
if [ $# -lt 1 ]; then
  echo "Error: Num args incorrecte"
  echo "Usage: $0 llista dir" 
  exit $ERR_NARGS
fi

# validar si es un dir 

lista_arg=$* 

for dir in $lista_arg
do 
  if [ ! -d $dir  ]; then
    echo "ERROR: $dir no és un directori" 2>>/dev/null
  else
    lista_dir=$(ls $dir)
    echo "dir: $dir" 
    for fit in $lista_dir
    do
      if [ -h "$dir/$fit" ]; then
        echo -e  "\t$fit és un link"
      elif [ -d "$dir/$fit" ]; then
        echo -e "\t$fit és un dir"
      elif [ -f "$dir/$fit" ]; then
        echo -e "\t$fit és un regular file"
      else
        echo -e "\t$fit és una altra cosa"	
      fi
    done
  fi
done

exit 0 

