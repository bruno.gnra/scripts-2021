#! /bin/bash
# @edt ASIX-M01 CURS 2021-2022
# Febrero 14
# Descripción: nota ...
# rep almenys una nota o mas 
# per cada nota diu si es sus, aprovat, notable 
# nota [0-10]
# ---------------------------------

ERR_NARGS=1
ERR_NRANG=2


# validar num de args 

if [ $# -lt 1 ]; then 
  echo "Error: Numero de args incorrecto" 
  echo "Usage: $0 nota" 
  exit $ERR_NARGS
fi

lista_notas=$*

for nota in $lista_notas
do
# validem rango de nota 
  if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
    echo "Error: Nota $nota no valid [0-10]" >> /dev/stderr #&2
    # echo "Usage: $0 nota"
# determinar el valor  
  elif [ $nota -lt 5 ]; then
    echo "La nota $nota es un Suspes"
  elif [ $nota -lt 7 ]; then
    echo "la nota $nota es un Aprovat"
  elif [ $nota -lt 9 ]; then
    echo "La nota $nota es un Notable"
  else
      echo "La nota $nota es un Excellent"
  fi  
done
exit 0 

