
#! /bin/bash
# @edt ASIX-M01 CURS 2021-2022
# Febrero 14
# Descripción: 

# validar 

if [ $# -lt 1 ]; then
  echo "Error: Numero de args incorrecto" 
  echo "Usage: $0 for arg" 
  exit $ERR_NARGS
fi 

# 
opcions=""
arguments=""

for arg in $* 
do 
    case $arg in 
	    "-a"|"-b"|"-c"|"-d"|"-e"|"-f")
		    opcions="$opcions $arg";;
            *) 
	    	    arguments="$arguments $arg";;
   esac 
done

echo "Opcions: $opcions" 
echo "Arguments: $arguments" 
exit 0 


