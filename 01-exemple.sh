#! /bin/bash
# @edt ASIX-M01
# Febrer 2022
# 
# Example de primer programa
# Normas: 
# 	shebang
# 	capcalera: descripció, data, autor 
#----------------------------------------------
# es pot fer tot allò que es fa a la línea de comandes 

echo "Hello World" 
nom='pere pou prat'
edat=25

echo $nom $edat 

echo -e "nom: $nom\n edat: $edat\n" 
echo -e 'nom: $nom\n edat: $edat\n'
uname -a 
uptime

echo $SHLVL
echo $SHELL
echo $((4*32))
echo $((edat*3))
#read data1 data2  
#echo -e "$data1 \n $data2"
exit 0 
