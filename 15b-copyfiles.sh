#! /bin/bash
# @edt ASIX-M01 CURS 2021-2022
# Febrero 14
# Descripción: file origen dirdesti 

# validar 2 args
# primer arg es un file 
# segon es un dir existen 
#-------------------------------
$ERR_NARGS=1
$ERR_NDIR=2
$OK=0 

# validem args 

if [ $# -lt 2 ] 
then 
  echo "Error: Numero de args incorrecto" 
  echo "Usage: $0 file dir-desti" 
  exit $ERR_NARGS
fi 

# magia 
#dir=$(echo $* | sed 's/^.*//')
dir=$(echo $* | cut -d' ' -f$#) 

#llista_file=$(echo $* | sed 's/ [^ ]*$//' 
llista_file=$(echo $* | cut -d' ' -f1-$(($#-1))

echo "$dir"
echo "$lista_file"

exit 0
# validar si es un dir 
dir=$2

if [ ! -d $2 ] 
then 
  echo "ERROR: $dir no és un directori" 2>>/dev/null
  echo "Usage: $0 file dir-desti" 
  exit $ERR_NDIR
fi 

# Iterar file a file 

for $file in $llista_files
do 
  if [ !-f $file ]
  then
    echo "Error: No es regular file"  2>>/dev/null
  else  
    copy $file $dir 
  fi 
done 

exit OK 
