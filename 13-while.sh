#! /bin/bash
# @edt ASIX-M01 CURS 2021-2022
# Febrero 14
# Descripción: exemples bucle while 
# Mostrar un comptador del 1 a MAX


# 7) numerar stdin linea a linea i passar a majusculas 
num=1 
while read -r line 
do 
  echo "$num:$line " | tr 'a-z' 'A-Z'
  ((num++))
done 
exit 0 

# 6) processar stdin fins al token FI

read -r line 
while [ $line != "FI" ]
do 
    echo "$line"  
    read -r line  
done
exit 0 

# 5) Numerar stdin linea a linea 

num=1 

while read -r line 
do 
  echo "$num: $line"
  ((num++))
done
exit 0

# 4) Processar l'entrada estandar, linea a linea 

while read -r line
do 
  echo $line
done 
exit 0 

# 3) iterar arguments amb shift 
 
while [ -n "$1" ]
do
  echo "$1,$#,$*"
  shift
done 
exit 0

# 2) contador decreixen del arg [N-0]

MIN=0 

num=$1 

while [ $num -ge $MIN ]
do 
  echo -n "$num, " # -n sirve para que no haga el salot de linea 
  ((num--))
done 
exit 0 

#---------------------------------
MAX=10 

num=1 
while [ $num -le $MAX ]
do 
  echo "$num"
  ((num++))
done 
exit 0 

