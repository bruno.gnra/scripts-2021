#! /bin/bash
# @edt ASIX-M01
# Febrer 2022

# validar existeix un arg
# dir es un directori 
# fer un ls del directori
# numerar un a un els noms de fixers 

ERR_NARGS=1
ERR_NODIR=2 

# validar num args 
if [ $# -ne 1 ]; then
  echo "Error: Num args incorrecte"
  echo "Usage: $0 llista dir" 
  exit $ERR_NARGS
fi

# validar si es un dir 

if [ ! -d $1  ]; then
  echo "ERROR: $1 no és un directori"
  echo "usage: $0 llista dir"
  exit $ERR_NODIR
fi 

# xixa 

dir=$1 
lista_dir=$(ls $dir)

for fit in $lista_dir
do
  if [ -h "$dir/$fit" ]; then
    echo "$fit és un link"
  elif [ -d "$dir/$fit" ]; then
    echo "$fit és un dir"
  elif [ -f "$dir/$fit" ]; then
    echo "$fit és un regular file"
  else
    echo "$fit és una altra cosa"	
fi
done 
exit 0 

